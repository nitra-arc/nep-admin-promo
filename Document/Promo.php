<?php

namespace Nitra\PromoBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ODM\Document
 */
class Promo
{
    /**
     * @var string Identifier
     * @ODM\Id
     */
    protected $id;

    /**
     * @var string Name
     * @ODM\String
     * @Assert\NotBlank
     * @Assert\Length(max="255")
     * @Gedmo\Translatable
     */
    protected $name;

    /**
     * @var string Url
     * @ODM\String
     * @Assert\Length(max="255")
     */
    protected $link;

    /**
     * @var string Image path
     * @ODM\String
     * @Assert\Length(max="255")
     */
    protected $image;

    /**
     * @var string Description
     * @ODM\String
     * @Gedmo\Translatable
     */
    protected $description;

    /**
     * @var integer Sort order
     * @ODM\Int
     */
    protected $sortOrder;

    /**
     * @var \Nitra\StoreBundle\Document\Store[] Reference stores
     * @ODM\ReferenceMany(targetDocument="Nitra\StoreBundle\Document\Store")
     */
    protected $stores;

    /**
     * To string converter
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->stores = new ArrayCollection();
    }

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set link
     * @param string $link
     * @return self
     */
    public function setLink($link)
    {
        $this->link = $link;
        return $this;
    }

    /**
     * Get link
     * @return string $link
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set image
     * @param string $image
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * Get image
     * @return string $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set description
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set sortOrder
     * @param int $sortOrder
     * @return self
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     * Get sortOrder
     * @return int $sortOrder
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Add stores
     * @param \Nitra\StoreBundle\Document\Store $stores
     * @return self
     */
    public function addStores(\Nitra\StoreBundle\Document\Store $stores)
    {
        $this->stores[] = $stores;
        return $this;
    }

    /**
     * Get stores
     * @return \Nitra\StoreBundle\Document\Store[] $stores
     */
    public function getStores()
    {
        return $this->stores;
    }

    /**
     * Set stores
     * @param \Nitra\StoreBundle\Document\Store[] $stores
     * @return self
     */
    public function setStores($stores)
    {
        $this->stores = $stores;
        return $this;
    }
}